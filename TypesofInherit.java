import java.io.*;
class GrandPa{
    GrandPa(){
        System.out.println("in grandpa class");
    }
}
class Parent1{
    Parent1(){
    System.out.println("In Parent1 class");
    }
}
class Parent2 extends GrandPa{
    Parent2(){
        System.out.println("In Parent2 class extends grandpa");
    }
}
class Parent3{
    Parent3(){
        System.out.println("In Parent3 class");
    }
}
class Child1 extends Parent1{
    Child1(){
        System.out.println("In Child1 class extends Parent1");
    }
}
class Child2 extends Parent2{
    Child2(){
        System.out.println("In Child2 class extends Parent2");
    }
}
class Child3 extends Parent3{
    Child3(){
        Parent1 objj =new Parent1();
        System.out.println("In Child3 class extends Parent3");
        System.out.println("this is multiple inheritance(?)");
    }
}
class Main{
    public static void main(String[] args){
        Child1 obj1= new Child1();
        System.out.println("This is Single Inheritance\n\n");
        Child2 obj2= new Child2();
        System.out.println("This is multi level inheritance ");
        System.out.println(" ");
        Child3 obj3=new Child3();
    }      
}
